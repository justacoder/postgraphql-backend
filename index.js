const Identity = require('fake-identity');

const { Pool } = require('pg')
const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "oz",
  password: "",
  port: 5432
});

const queryCallback = function(err, payload) {
  if(err) {
    console.error(err);
  }

  console.info(JSON.stringify(payload, null, 4));
}

const prepareUserRecord = function(obj) {
  return [
    obj.firstName,
    obj.lastName,
    obj.emailAddress,
    obj.dateOfBirth,
    obj.street,
    obj.city,
    obj.state,
    obj.zipCode,
    obj.phoneNumber,
  ]
}

const createUserRecord = function(identity) {
  return new Promise((resolve, reject) => {
    pool.query('insert into public.user (first_name, last_name, email, date_of_birth, contact) values ($1, $2, $3, $4, ($5, $6, $7, $8, $9))', prepareUserRecord(identity), (err, payload) => {
      if(err) {
        reject(err);
      }

      resolve(payload);
    });
  });
}
let i;
const users = [];
for(i=0; i<99; i++) {
  users.push(Identity.generate());
}
const chain = users.map((user) => createUserRecord.bind(null, user)).reduce((prev, next) => prev.then(next), Promise.resolve());
chain.then(() => {
pool.end(queryCallback);
});
